package com.ealanta.kotlindemo

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.http.MediaType
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.util.concurrent.atomic.AtomicLong
import java.util.function.Supplier
import java.util.stream.Stream

@SpringBootApplication
class KotlindemoApplication {

    @Profile("!test")
    @Bean()
    fun initialiser(repo: PersonRepository): CommandLineRunner = CommandLineRunner {
        val dobClark: LocalDate = LocalDate.of(1901, Month.JANUARY, 1)
        val dobBruce: LocalDate = LocalDate.of(1902, Month.FEBRUARY, 2)

        val peeps = Flux.just(
                Person("001", "Bat"  , "Man", dobBruce),
                Person("002", "Super", "Man", dobClark));
        val saved = peeps.flatMap { it -> repo.save(it) }
        val found = saved.thenMany(repo.findAll());

        found.subscribe { it -> System.out.println("From DB $it") }
    }
}

fun main(args: Array<String>) {
    runApplication<KotlindemoApplication>(*args)
}

@RestController
class PersonController(private val repo: PersonRepository) {

    @GetMapping("/person", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getAll(): Flux<Person> {
        return repo.findAll();
    }

    @GetMapping("/person/{id}", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getById(@PathVariable("id") id: String): Mono<Person> {
        return repo.findById(id);
    }

    @GetMapping(value = "/events", produces = arrayOf(MediaType.TEXT_EVENT_STREAM_VALUE))
    fun getEvents(): Flux<Event> {
        val counter: AtomicLong = AtomicLong(1);
        val eventFlux: Flux<Event> = Flux.fromStream(Stream.generate(Supplier { Event(counter.getAndIncrement(), LocalDateTime.now()) }))
        //val durationFlux: Flux<Long> = Flux.interval(Duration.ofSeconds(1));
        //val delayedEventFlux = Flux.zip(durationFlux, eventFlux).map { it -> it.t2 }
        val delayedEventFlux = eventFlux.delayElements(Duration.ofSeconds(1))
        return delayedEventFlux;
    }
}

@Document
data class Person(val id: String, val first: String, val last: String, val dateOfBirth: LocalDate? = null, val address: Address? = null)

//I like this
data class Event(val id: Long, val timestamp: LocalDateTime) {
    constructor(id: Long) : this(id, LocalDateTime.now())
}

data class Address(
        val addressLine1: String,
        val addressLine2: String?,
        val town: String,
        val county: String?,
        val postcode: String
)

@Repository
interface PersonRepository : ReactiveCrudRepository<Person, String>

