package com.ealanta.kotlindemo

import org.junit.Test
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import reactor.test.scheduler.VirtualTimeScheduler
import java.time.Duration
import kotlin.test.assertTrue

class StepVerifierTests {

    @Test
    fun testStepVerifierWithRealTime() {
        val dataFlux = Flux.just(1, 2, 3, 4, 5);
        val delayedDataFlux = dataFlux.delayElements(Duration.ofSeconds(1));

        val duration = StepVerifier.create(delayedDataFlux)
                .assertNext { it -> it == 1 }
                .assertNext { it -> it == 2 }
                .assertNext { it -> it == 3 }
                .assertNext { it -> it == 4 }
                .assertNext { it -> it == 5 }
                .verifyComplete();
        assertTrue { duration.toMillis() >= 5000L }
    }

    @Test
    fun testStepVerifierWithVirtualTime() {

        val scheduler = VirtualTimeScheduler.getOrSet()
        val dataFlux = Flux.just(1, 2, 3, 4, 5);
        val delayedDataFlux = dataFlux.delayElements(Duration.ofSeconds(1));

        val duration = StepVerifier.withVirtualTime({ delayedDataFlux })
                .expectSubscription()

                .expectNoEvent(Duration.ofSeconds(1))//first event comes after a second
                .assertNext { 1 }

                .thenAwait(Duration.ofSeconds(1))//we can wait another second and get another event
                .assertNext { 2 }

                .thenAwait(Duration.ofSeconds(2)) //advance 2 seconds
                .assertNext { 3 }
                .assertNext { 4 }

                .expectNoEvent(Duration.ofSeconds(1)) //no event for another second
                .assertNext { 5 }


                .verifyComplete();
        System.out.println("Duration with virtual time $duration")
    }

}