package com.ealanta.kotlindemo

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RunWith(SpringRunner::class)
@WebMvcTest(value = PersonController::class)
@ActiveProfiles("test")
class PetControllerTest {

    @Autowired
    lateinit private var mockMvc: MockMvc

    @MockBean
    lateinit var mockRepo: PersonRepository;

    @Before
    fun setup() {
        val peepsFlux = Flux.just(
                Person(id = "ONE", first = "fOne", last = "lOne"),
                Person(id = "TWO", first = "fTwo", last = "lTwo")
        );
        val personMono = Mono.just(
                Person(id = "123", first = "FIRST", last = "LAST")
        );
        Mockito.`when`(mockRepo.findAll()).thenReturn(peepsFlux);
        Mockito.`when`(mockRepo.findById("123")).thenReturn(personMono);
    }

    @Test
    fun testGetUsers() {
        val asyncResult = this.mockMvc
                .perform(get("/person")
                        .characterEncoding("UTF-8")
                        .accept(MediaType.APPLICATION_JSON)).andReturn();

        mockMvc
                .perform(asyncDispatch(asyncResult))
                .andDo(print()).andExpect(status().isOk)
                .andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json("[" +
                        "{'id':'ONE','first':'fOne','last':'lOne'}," +
                        "{'id':'TWO','first':'fTwo','last':'lTwo'}]"));

    }

    @Test
    fun testGetSpecificUser() {
        val asyncResult = this.mockMvc
                .perform(get("/person/123")
                        .characterEncoding("UTF-8")
                        .accept(MediaType.APPLICATION_JSON)).andReturn();

        mockMvc
                .perform(asyncDispatch(asyncResult))
                .andDo(print()).andExpect(status().isOk)
                .andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json("{'id':'123','first':'FIRST','last':'LAST'}"));

    }
}
