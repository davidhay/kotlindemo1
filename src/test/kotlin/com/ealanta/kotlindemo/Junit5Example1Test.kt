package com.ealanta.kotlindemo

import org.junit.jupiter.api.*
import java.io.Closeable

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Junit5Example1Test {

    lateinit var calculator: Calculator;

    @BeforeAll //non-static before all
    fun beforeAll(){
        this.calculator = Calculator("calc-one");
    }

    @Test
    fun `Adding 1 and 3 should be equal to 4`() {
        Assertions.assertEquals(4, calculator.add(1, 3))
    }

    @AfterAll //non-static after-all
    fun afterAll(){
        this.calculator.close();
    }
}

class Calculator(val name:String) : Closeable {

    fun add(left:Int,right:Int):Int {
        return left+right;
    }
    override  fun close() {
        System.out.println("$this closed.")
    }

    override fun toString():String {
        return "Calculator($name)"
    }
}