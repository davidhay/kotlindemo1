package com.ealanta.kotlindemo

import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult
import reactor.core.publisher.toMono
import reactor.test.StepVerifier
import java.nio.charset.Charset
import java.time.Duration
import java.time.LocalDate
import java.time.Month
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail


class ReactiveClientTest {

    fun webTestClient(): WebTestClient {
        return WebTestClient.bindToServer().baseUrl("http://localhost:8080").build()
    }

    @Test
    fun testEvents(): Unit {
        val result = webTestClient().get().uri("/events")
                .accept(MediaType.TEXT_EVENT_STREAM)
                .acceptCharset(Charset.forName("utf-8"))
                .exchange().expectStatus().isOk().returnResult<Event>();

        val eventFlux = result.getResponseBody()

        val start = System.currentTimeMillis();

        val events: List<Event>? = eventFlux.take(6).collectList().block(Duration.ofSeconds(6))
        events ?: fail("exepcted a non null list")
        assertEquals(6, events.size)
        val diff = System.currentTimeMillis() - start;
        System.out.println("DIFF IS $diff")
        assertTrue { diff > 5500L }
        for ((idx, evt) in events.withIndex()) {
            assertEquals(idx + 1L, evt.id)
        }
    }

    @Test
    fun testPeople(): Unit {
        val result: FluxExchangeResult<Person> = webTestClient().get().uri("/person")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange().expectStatus().isOk().returnResult<Person>();

        val peopleFlux = result.getResponseBody()

        StepVerifier
                .create<Person>(peopleFlux)
                .expectNext(Person("001", "Bat", "Man", LocalDate.of(1902, Month.FEBRUARY, 2)))
                .expectNext(Person("002", "Super", "Man", LocalDate.of(1901, Month.JANUARY, 1)))
                .expectComplete()
                .verify()
    }

    @Test
    fun testPerson001(): Unit {
        checkPerson("/person/001", Person("001", "Bat", "Man", LocalDate.of(1902, Month.FEBRUARY, 2)))
    }

    @Test
    fun testPerson002(): Unit {
        checkPerson("/person/002", Person("002", "Super", "Man", LocalDate.of(1901, Month.JANUARY, 1)))
    }

    fun checkPerson(uri: String, expected: Person): Unit {
        val result: FluxExchangeResult<Person> = webTestClient().get().uri(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange().expectStatus().isOk().returnResult<Person>();

        val peopleMono = result.responseBody.toMono();

        StepVerifier
                .create<Person>(peopleMono)
                .expectNext(expected)
                .expectComplete()
                .verify()
    }
}
